#include "costoparqueadero.h"

CostoParqueadero::CostoParqueadero(int parqueadero, int costo)
{
    this->parqueadero = parqueadero;
    this->costo = costo;
}

int CostoParqueadero::getCosto() const
{
    return costo;
}

int CostoParqueadero::getParqueadero()
{
    return parqueadero;
}

void CostoParqueadero::setCosto(int costo)
{
    this->costo = costo;
}

void CostoParqueadero::setParqueadero(int parqueadero)
{
    this->parqueadero = parqueadero;
}

bool CostoParqueadero::operator<(const CostoParqueadero &otro) const
{
    return (this->costo < otro.getCosto());
}
