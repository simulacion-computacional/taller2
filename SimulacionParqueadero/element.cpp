#include "element.h"

Element::Element()
{
    this->priority=7890987;
    this->name="";
}

Element::Element(int priority, string nombre)
{
    this->priority = priority;
    this->name = nombre;
}

void Element::setPriority(int priority)
{
    this->priority = priority;
}

void Element::setName(string name)
{
    this->name = name;
}

int Element::getPriority()
{
    return priority;
}

string Element::getName()
{
    return name;
}
