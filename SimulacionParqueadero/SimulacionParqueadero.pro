#-------------------------------------------------
#
# Project created by QtCreator 2011-10-01T10:52:09
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = SimulacionParqueadero
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    heap.cpp \
    element.cpp \
    puntoTres.cpp \
    parqueadero.cpp \
    cliente.cpp \
    puntoDos.cpp \
    costoparqueadero.cpp \
    puntoUno.cpp

HEADERS += \
    heap.h \
    element.h \
    parqueadero.h \
    cliente.h \
    costoparqueadero.h
