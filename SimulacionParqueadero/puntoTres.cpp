#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::string;
using std::ofstream;

int main()
{
    srand(time(NULL));

    string nombre;
    cout << "Ingrese la ruta donde quedara el archivo generado: ";
    cin >> nombre;

    ofstream out;
    out.open(nombre.c_str());

    int L;
    int P;
    int E;

    cout << "Cantidad de locales: ";
    cin >> L;
    cout << "Cantidad de parqueaderos: ";
    cin >> P;
    cout << "Cantidad de eventos a generar: ";
    cin >> E;

    out << L << " " << P << " " << "K" << endl;

    for(int i=0;i<L;i++)
    {
        for(int j=0;j<P;j++)
        {
            if(j!=P-1)
            {
                out << (rand()%1000000 + 1) << " ";
            }else
            {
                out << (rand()%1000000 + 1); //distancias entre 1 y 1 millon
            }
        }
        out << endl;
    }

    vector<int> tiempos;
    for(int i=0;i<E;i++)
    {
        int valor = rand()%1000001; //numero entre 0 y 1000000
        tiempos.push_back(valor);
    }

    sort(tiempos.begin(), tiempos.end()); //ordenar los tiempos generados

    for(int i=0;i<tiempos.size();i++)
    {
        vector<int> locales;//contiene todos los locales, lo usaré para no repetir seleccion de local
        for(int k=0;k<L;k++)
        {
            locales.push_back(k);
        }

        out << tiempos.at(i) << " "; //tiempo de llegada
        out << rand()%10000000 + 1 << " "; //tiempo de duracion, numero entre 1 y 1 millon
        int cantidadLocales = rand()%L + 1;
        out << cantidadLocales << " "; //cantidad de locales que visitara, numero entre 1 y L
        for(int j=cantidadLocales;j>0;j--)
        {
            int index = rand()%locales.size();

            if(j != 1)
            {
                out << locales.at(index) << " ";
                locales.erase(locales.begin()+index);
            }else
            {
                out << locales.at(index); //numero entre 0 y L-1, los locales que visitará
                locales.erase(locales.begin()+index);
            }
        }
        out << endl;
    }

    out << "END";
    out.close();

    return 0;
}
