#ifndef ELEMENT_H
#define ELEMENT_H
#include <iostream>
using namespace std;

class Element
{
private:
    int priority;
    string name;
public:
    Element();
    Element(int priority, string nombre);
    void setPriority(int priority);
    void setName(string name);
    int getPriority();
    string getName();
};

#endif // ELEMENT_H
