#include "parqueadero.h"
#include "costoparqueadero.h"
#include <string>
#include <iostream>
#include <algorithm>

using std::vector;
using std::string;
using std::cout;
using std::endl;

Parqueadero::Parqueadero(int L, int P, int K, vector<vector<int> > distancias)
{
    this->L = L;
    this->P = P;
    this->K = K;
    this->distancias = distancias;
    pLibres = new bool[P];
    for(int i=0;i<P;i++)
    {
        pLibres[i] = true;
    }
    idConsecutivo = 1;
    srand(time(NULL));
}

Parqueadero::~Parqueadero()
{
    delete pLibres;
    pLibres = 0;
}

void Parqueadero::llegada(vector<int> eventoIngreso)
{
    int tiempo = eventoIngreso.at(0);
    if(tiempo == -1)
    {
        continuarSimulacion();
    }else
    {
        int duracion = eventoIngreso.at(1);
        int cantidadLocales = eventoIngreso.at(2);
        vector<int> localesParaVisitar;
        for(int i=0;i<cantidadLocales;i++)
        {
            localesParaVisitar.push_back(eventoIngreso.at(i+3));
        }
        calcularCosto(localesParaVisitar, cantidadLocales);
        if(costo == -1)
        {
            cout << "unavailable" << endl;
        }else
        {
            Cliente c("IN",tiempo,idConsecutivo,ps,costo);
            cola.push(c);
            Cliente s("OUT",tiempo+duracion,idConsecutivo,ps,costo);
            cola.push(s);
            idConsecutivo++;
            ejecutarEvento();
        }
    }
}

void Parqueadero::calcularCosto(vector<int> localesParaVisitar, int cantiadLocales)
{
    int sum = 0;
    vector<CostoParqueadero> posibles;

    for(int i=0;i<P;i++)
    {
        if(pLibres[i])//solo tenemos en cuenta los parqueaderos disponible
        {
            for(int j=0;j<cantiadLocales;j++)
            {
                int index = localesParaVisitar.at(j);
                sum += distancias.at(index).at(i)*2; //ida y regreso
            }
            CostoParqueadero c(i,sum);
            posibles.push_back(c);
            sum = 0;
        }
    }

    if(posibles.size() == 0)
    {
        costo = -1;
    }else
    {
        sort(posibles.begin(), posibles.end()); //ordenamos las posibles parqueaderos
        int index = K;
        while(index>posibles.size()) //por si hay mas k posibles que la cantidad real de parqueadero
            index--;
        index = rand()%index;  //de los k mejores(en lo posible), obtenemos uno aleatoriamente
        costo = posibles.at(index).getCosto() ;
        ps = posibles.at(index).getParqueadero();
        pLibres[ps] = false; //ahora esta ocupado
    }
}

void Parqueadero::continuarSimulacion()
{
    while(!cola.empty())
    {
        ejecutarEvento();
    }
}

void Parqueadero::ejecutarEvento()
{
    Cliente c = cola.top();
    string estado = c.getEstado();
    if(estado == "IN")
    {
        cout << estado << " ";
        cout << c.getTiempo() << " ";
        cout << c.getId() << " ";
        cout << c.getParqueadero() << " ";
        cout << c.getCosto() << endl;
    }else //estado es OUT
    {
        cout << estado << " ";
        cout << c.getTiempo() << " ";
        cout << c.getId() << " ";
        cout << c.getParqueadero() << endl;
        pLibres[c.getParqueadero()] = true; //el parqueadero queda libre
    }
    cola.pop(); //saco el elemento de la cola
}
