#include "cliente.h"

using std::string;

Cliente::Cliente(std::string estado, int tiempo, int id, int parqueadero, int costo)
{
    this->estado = estado;
    this->tiempo = tiempo;
    this->id = id;
    this->parqueadero = parqueadero;
    this->costo = costo;
}

string Cliente::getEstado()
{
    return estado;
}

int Cliente::getTiempo() const
{
    return this->tiempo;
}

int Cliente::getId()
{
    return id;
}

int Cliente::getParqueadero()
{
    return parqueadero;
}

int Cliente::getCosto()
{
    return costo;
}

void Cliente::setEstado(string estado)
{
    this->estado = estado;
}

void Cliente::setId(int id)
{
    this->id = id;
}

void Cliente::setParqueadero(int parqueadero)
{
    this->parqueadero = parqueadero;
}

void Cliente::setCosto(int costo)
{
    this->costo = costo;
}

void Cliente::setTiempo(int tiempo)
{
    this->tiempo = tiempo;
}

bool Cliente::operator>(const Cliente &otro) const
{
    return (this->tiempo > otro.getTiempo());
}
