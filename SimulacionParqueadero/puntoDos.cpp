#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>
#include "parqueadero.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;

int main()
{
    int L;
    int P;
    int K;

    cin >> L;
    cin >> P;
    cin >> K;

    vector<vector<int> > distancias;//L filas

    int temp;//para almacenar int temporalmente

    for(int i=0;i<L;i++)
    {
        vector<int> p;
        for(int j=0;j<P;j++)
        {
            cin >> temp;
            p.push_back(temp);
        }
        distancias.push_back(p);
    }

    Parqueadero parqueadero(L,P,K,distancias);

    vector<int> eventoIngreso;//L+3 es la maxima longitud de una linea de evento de entrada
    string tiempo;
    cin >> tiempo;
    while(tiempo != "END")
    {
        eventoIngreso.push_back(atoi(tiempo.c_str())); //timepo de llegada del cliente
        cin >> temp;
        eventoIngreso.push_back(temp); //duracion estadia
        cin >> temp;
        eventoIngreso.push_back(temp); //cantidad de locales a visitar
        for(int i=0;i<eventoIngreso.at(2);i++)
        {
            cin >> temp;
            eventoIngreso.push_back(temp); //locales a visitar
        }
        parqueadero.llegada(eventoIngreso);
        cin >> tiempo;
        eventoIngreso.clear();
    }
    eventoIngreso.push_back(-1);
    parqueadero.llegada(eventoIngreso);

    return 0;
}
