#include "heap.h"
#include <iostream>
using namespace std;

Heap::Heap(int size, Element *array, int n)
{
    this->size = size;
    this->array = new Element[this->size];
    this->heapSize = 0;
    for(int i=0;i<n;i++)
    {
        this->array[i] = array[i];
        this->heapSize++;
    }
    delete [] array;
    array = 0;
}

void Heap::heapify(int nodo)
{
    int leftIndex= 2*nodo -1;
    int rightIndex= 2*nodo;

    Element left;
    Element right;
    if(leftIndex <= this->heapSize & rightIndex <= this->heapSize)
    {
        left= this->array[leftIndex];
        right= this->array[rightIndex];
    }

    int newNodo;
    if ((leftIndex < this->heapSize) & (left.getPriority() < this->array[nodo-1].getPriority()))
    {
        newNodo = 2*nodo;
    }else
    {
        newNodo=nodo;
    }

    if((rightIndex< this->heapSize) & (right.getPriority() < this->array[newNodo-1].getPriority()))
    {
        newNodo= 2*nodo + 1;
    }

    if (newNodo != nodo)
    {
        Element tem(this->array[nodo -1].getPriority(),this->array[nodo-1].getName());
        this->array[nodo-1] =this->array[newNodo-1];
        this->array[newNodo-1]= tem;
        this->heapify(newNodo);
    }

    return;

}

void Heap::buildHeap()
{
    for(int i= int (this->heapSize / 2); i>=1; i--)
    {
        this->heapify(i);
    }

}

void Heap::copyArray()
{
    for(int i= 0 ; i< this->heapSize; i++)
    {
        cout<<this->array[i].getName()<<" ";

    }
    cout<<"\n";
}

void Heap::heapExtract()
{
    int minimo= this->array[0].getPriority();
    Element tem(this->array[0].getPriority(), this->array[0].getName());
    this->array[0]= this->array[this->heapSize-1];
    this->array[this->heapSize-1]= tem;

    this->heapSize--;
    heapify(1);
    this->array[this->heapSize]= Element();
    return;
}

void Heap::heapInsert(Element elem)
{
    this->heapSize++;
    this->array[this->heapSize-1]= elem;
    int i= this->heapSize-1;
    while(i>=1)
    {
        if(this->array[int(i/2)].getPriority() < this->array[i].getPriority())
        {
            return;
        }else{
            Element tem(this->array[i].getPriority(), this->array[i].getName());
            this->array[i]= this->array[int(i/2)];
            this->array[int(i/2)]= tem;

            i= int(i/2);

        }

    }
    return;

}
